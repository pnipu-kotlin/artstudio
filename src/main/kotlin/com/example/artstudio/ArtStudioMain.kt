package com.example.artstudio



import com.example.artstudio.controllers.messager.TerminalMessager
import com.example.artstudio.presenters.MenuActions
import javafx.application.Application
import javafx.event.EventHandler
import javafx.fxml.FXMLLoader
import javafx.scene.Scene
import javafx.scene.image.Image
import javafx.stage.Stage


class ArtStudioMain : Application() {

    override fun start(stage: Stage) {

        val cssFile = ArtStudioMain::class.java.getResource("css/styles.css")
        cssFile?.let{
            SCENE.stylesheets.add(it.toString())
        }

        with(stage) {
            title = "ArtStudio"
            scene = SCENE
            minHeight = SCENE_MIN_HEIGHT
            minWidth = SCENE_MIN_WIDTH
            SCENE.onKeyTyped = EventHandler{event->

                event.character.chars().forEach{
                    if (it == 127){
                        MenuActions.deleteAction()
                    }
                }
            }
        }

        try{
            val iconStream = ArtStudioMain::class.java.getResourceAsStream("img/art.jpg")
            val img = Image(iconStream)
            stage.icons.add(img)
        }catch(e: Exception){
            log.error("Файл изображения логотипа не найден")
            log.error(e.message.toString())
        }
        stage.show()
    }

    private companion object {
        const val SCENE_START_WIDTH = 640.0
        const val SCENE_START_HEIGHT = 480.0
        const val SCENE_MIN_WIDTH = 300.0
        const val SCENE_MIN_HEIGHT = 200.0
        val fxmlLoader = FXMLLoader(ArtStudioMain::class.java.getResource("hello-view.fxml"))
        val SCENE = Scene(fxmlLoader.load(), SCENE_START_WIDTH, SCENE_START_HEIGHT)
        val log = TerminalMessager()
    }
}
