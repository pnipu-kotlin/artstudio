package com.example.artstudio.storage

import com.example.artstudio.controllers.messager.PaneMessager
import com.example.artstudio.controllers.messager.TerminalMessager
import com.example.artstudio.presenters.fxutils.FXUtils
import javafx.scene.image.WritableImage
import java.io.File
import java.io.IOException
import javax.imageio.ImageIO

class FileStorage: IStorageProvider {

    override fun load(fileName: String): Pair<List<String>, IOStatus> =
        try{
            File(fileName).readLines() to IOStatus.OK
        }catch(ioe: IOException){
            log.error("Ошибка чтения файла : ${ioe.message}")
            mutableListOf<String>() to IOStatus.IO_ERROR
        }

    override fun save(fileName: String, data: List<String>): IOStatus {

        if (fileName.length < MIN_FILENAME_SIZE){
            msg.error("Название файла не указано или слишком короткое")
            return IOStatus.FILENAME_INCORRECT
        }
        try{
            File(fileName).printWriter().use { out ->
                data.forEach { figStr ->
                    out.println(figStr)
                }
            }

        }catch(e:Exception){
            msg.error("Ошибка сохранения файла: ${e.message.toString()}")
            log.error("Ошибка сохранения файла: ${e.message.toString()}")
            return IOStatus.IO_ERROR
        }
        return IOStatus.OK
    }

    fun exportToPng(fileName: File, img: WritableImage){
        try{
            ImageIO.write(FXUtils.fromFXImage(img, null), "png", fileName)
        }catch(ioe: IOException){
            ioe.printStackTrace()
        }
    }

    private companion object {

        private const val MIN_FILENAME_SIZE = 5
        private val log = TerminalMessager()
        private val msg = PaneMessager()
    }
}