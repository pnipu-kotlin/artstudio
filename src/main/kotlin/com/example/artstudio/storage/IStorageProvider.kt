package com.example.artstudio.storage

interface IStorageProvider {

    fun load(fileName: String) :Pair<List<String>?, IOStatus>

    fun save(fileName: String, data: List<String>) :IOStatus

}