package com.example.artstudio.storage

enum class IOStatus {
    OK,
    FILENAME_INCORRECT,
    IO_ERROR
}