package com.example.artstudio

import javafx.application.Application

fun main() = Application.launch(ArtStudioMain::class.java)
