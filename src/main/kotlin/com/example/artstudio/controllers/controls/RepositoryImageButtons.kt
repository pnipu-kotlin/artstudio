package com.example.artstudio.controllers.controls

import com.example.artstudio.controllers.entities.ModeType
import com.example.artstudio.controllers.entities.FigureType as fig
import javafx.scene.paint.Color as col

object RepositoryImageButtons {

    fun getShapeButtonList(): MutableList<ShapeButton> = mutableListOf(
        ShapeButton("select", "img/select.jpg", fig.RECTANGLE),
        ShapeButton("rect", "img/rect.jpg", fig.RECTANGLE),
        ShapeButton("circ", "img/circ.jpg", fig.ELLIPSE),
        ShapeButton("triang", "img/triang.jpg", fig.TRIANGLE),
        ShapeButton("star5", "img/star5.jpg", fig.STAR5),

        ShapeButton("line", "img/line.jpg", fig.LINE),
        ShapeButton("rect_filled", "img/rect_filled.jpg", fig.RECTANGLE),
        ShapeButton("circ_filled", "img/circ_filled.jpg", fig.ELLIPSE),
        ShapeButton("triang_filled", "img/triang_filled.jpg", fig.TRIANGLE),
        ShapeButton("star5_filled", "img/star5_filled.jpg", fig.STAR5)
    )


    fun getColorButtonList(): MutableList<ColorButton> = mutableListOf(
        ColorButton("black", col.BLACK),
        ColorButton("grey", col.web("#808080")),
        ColorButton("lightgrey", col.web("#d3d3d3")),
        ColorButton("brown", col.BROWN),
        ColorButton("darkred", col.web("#8b0000")),
        ColorButton("red", col.RED),
        ColorButton("orange", col.ORANGE),
        ColorButton("yellow", col.YELLOW),
        ColorButton("gold", col.GOLD),
        ColorButton("lime", col.LIME),
        ColorButton("green", col.GREEN),
        ColorButton("cyan", col.CYAN),
        ColorButton("blue", col.BLUE),
        ColorButton("magenta", col.MAGENTA),
        ColorButton("indigo", col.INDIGO),
        ColorButton("orchid", col.ORCHID),
        ColorButton("pink", col.PINK),
        ColorButton("white", col.WHITE)
    )

    fun getModeButtonList(): MutableList<ModeButton> = mutableListOf(
        ModeButton("режим рисования", ModeType.DRAWING).apply{
            this.isSelected = true
        },
        ModeButton("морфинг", ModeType.MORPHING)
    )
}