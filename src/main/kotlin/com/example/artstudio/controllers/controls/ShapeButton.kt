package com.example.artstudio.controllers.controls

import com.example.artstudio.controllers.entities.FigureType

class ShapeButton(name: String, imgFileName: String, var typeFigure: FigureType) : ImageButton(name, imgFileName)