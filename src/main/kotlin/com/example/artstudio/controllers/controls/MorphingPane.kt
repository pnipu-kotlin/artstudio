package com.example.artstudio.controllers.controls

import com.example.artstudio.controllers.messager.PaneMessager
import com.example.artstudio.presenters.calcs.MathCalc
import com.example.artstudio.presenters.calcs.MorphingCalc
import javafx.event.Event
import javafx.event.EventHandler
import javafx.fxml.Initializable
import javafx.scene.control.Slider
import javafx.scene.layout.*
import javafx.scene.paint.Color
import javafx.scene.shape.Ellipse
import javafx.scene.shape.Polygon
import java.net.URL
import java.util.*

class MorphingPane : GridPane(), Initializable {

    val paneLeft by lazy { Pane() }
    private val paneCenter by lazy { Pane() }
    val paneRight by lazy { Pane() }
    private val paneBottom by lazy { VBox() }
    private val slider by lazy {
        Slider(0.0, 100.0, 0.0).apply {
            orientation = javafx.geometry.Orientation.HORIZONTAL
            blockIncrement = 5.0
            isSnapToTicks = true
            style = " -fx-padding: 30px;"
            setPrefSize(600.0, 10.0)
            val clickAction = EventHandler<Event> {
                sliderVal = value.toInt()
                setEqualityPointsCount()
            }
            onMouseClicked = clickAction
            onMouseDragged = clickAction
        }
    }
    private var sliderVal = 0

    override fun initialize(p0: URL?, p1: ResourceBundle?) {
        isVisible = false
        background = BACKGROUND_ORANGE
        columnConstraints.addAll(col1, col2, col3)
        rowConstraints.addAll(row1, row2)

        val gList = listOf(listOf(0, 0, 1, 1), listOf(1, 0, 1, 1), listOf(2, 0, 1, 1), listOf(0, 1, 3, 1))
        listOf(paneLeft, paneCenter, paneRight, paneBottom).withIndex().forEach { (i, pane) ->
            with(pane) {
                border = BORDER
                background = BACKGROUND_ORANGE
                add(pane, gList[i][0], gList[i][1], gList[i][2], gList[i][3])
            }
        }
        paneBottom.children.add(slider)
    }

    private fun Pane.checkEllipses() {
        if (children.first() is Ellipse) {
            val poly = MathCalc.ellipseToPoly(children.first() as Ellipse)
            children.clear()
            children.add(poly)
        }
    }
    private fun Pane.getCountPoints() : Int =
        if (children.isEmpty() || children.first() !is Polygon) { 0 }
        else { (children.first() as Polygon).points.size / 2}

    /**
     * Сначала проверим, нет ли здесь эллипса. Если есть - то каждый дробится на 100 точек.
     * Проверяем количество точек на фигурах справа и слева
     * если количество не совпадает - обрабатываем фигуры для соответствия перед морфингом
     *
     * Для чего находим наименьшее общее кратное для этих двух количеств (но не более 100)
     * Если удалось подобрать НОК - комбинируем соответствия между узловыми точками,
     * а потом между промежуточными (возможно пропорционально длинам сегментов)
     * Если ровного соответствия не нашлось - комбинируем соответствия приблизительно,
     * а остатки - как получится тоже приблизительно.
     * Главное - опознать узловые точки в обеих фигурах
     */
    private fun setEqualityPointsCount() {
        // Проверим эллипсы, если они есть - преобразуем в полигоны
        paneLeft.checkEllipses()
        paneRight.checkEllipses()
        // Сравним кол-во точек слева и справа,
        val countLeft = paneLeft.getCountPoints()
        val countRight = paneRight.getCountPoints()
        if (countLeft * countRight == 0) {
            msg.error("Фигура для морфинга отсутствует или имеет неизвестный тип")
            return
        }
        if (countLeft != countRight) {
            // Если количество точек не совпадает - найдем оптимальное для них
            var countCenter = MathCalc.nok(countLeft, countRight)
            if (countCenter > 100) {
                countCenter = 100
            }
            // Выравниваем количество точек справа и слева за уши
            if (!MorphingCalc.checkCount(paneLeft, countCenter) ||
                !MorphingCalc.checkCount(paneRight, countCenter)
            ) {
                msg.error("Фигура для морфинга отсутствует или имеет неизвестный тип")
                return
            }
        }
        val fig1 = paneLeft.children.first()
        val fig2 = paneRight.children.first()
        if (fig1 !is Polygon || fig2 !is Polygon) {
            msg.error("Фигура не является полигоном!")
            return
        }
        MorphingCalc.doMorphing(fig1, fig2, paneCenter, sliderVal)
    }

    private companion object {

        val BORDER = Border(BorderStroke(Color.ORANGE, BorderStrokeStyle.SOLID, null, BorderWidths(1.0)))
        val BACKGROUND_ORANGE = Background(BackgroundFill(Color.web("#ffeedd"), null, null))
        val msg = PaneMessager()
        val col1 = ColumnConstraints().apply { percentWidth = 33.0 }
        val col2 = ColumnConstraints().apply { percentWidth = 34.0 }
        val col3 = ColumnConstraints().apply { percentWidth = 33.0 }
        val row1 = RowConstraints().apply { percentHeight = 80.0 }
        val row2 = RowConstraints().apply { percentHeight = 20.0 }
    }
}