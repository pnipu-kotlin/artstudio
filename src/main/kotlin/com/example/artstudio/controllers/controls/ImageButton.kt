package com.example.artstudio.controllers.controls

import com.example.artstudio.ArtStudioMain
import javafx.scene.control.Button
import javafx.scene.image.Image
import javafx.scene.image.ImageView

open class ImageButton(var name: String, imgFileName: String) : Button("", null) {

    private var img: Image? = null
    private var imgActive: Image? = null
    private val view by lazy { ImageView() }
    private val defaultIoStream by lazy { ArtStudioMain::class.java.getResourceAsStream("img/select.jpg") }
    var isActive = false
        set(value) {
            view.image = if (value) imgActive else img
            field = value
        }

    init {
        val isFileNameCorrect = imgFileName.length >= MIN_FILENAME_LENGTH && imgFileName.endsWith(".jpg")
        val fileName = if (isFileNameCorrect) imgFileName else "img/select.jpg"
        val fileNameActive = fileName.substring(0, fileName.length - 4) + "_active.jpg"

        var iconStream = ArtStudioMain::class.java.getResourceAsStream(fileName)
        if (iconStream == null) {
            iconStream = defaultIoStream
        }
        img = Image(iconStream)
        with(view) {
            image = img
            fitHeight = ICON_HEIGHT
            fitWidth = ICON_WIDTH
        }

        iconStream = ArtStudioMain::class.java.getResourceAsStream(fileNameActive)
        if (iconStream == null) {
            iconStream = defaultIoStream
        }
        imgActive = Image(iconStream)


        this.graphic = view

    }

    override fun toString(): String = "ImageButton(name='$name', isActive=$isActive)"

    private companion object {
        // Размеры иконки
        const val ICON_HEIGHT = 25.0
        const val ICON_WIDTH = 25.0
        const val MIN_FILENAME_LENGTH = 5
    }

}