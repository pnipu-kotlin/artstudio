package com.example.artstudio.controllers.controls

import com.example.artstudio.controllers.ArtStudioData
import com.example.artstudio.controllers.entities.ModeType
import com.example.artstudio.presenters.calcs.MathCalc
import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.geometry.Insets
import javafx.scene.control.Button
import javafx.scene.control.Label
import javafx.scene.control.ToggleGroup
import javafx.scene.effect.InnerShadow
import javafx.scene.input.MouseEvent
import javafx.scene.layout.*
import javafx.scene.paint.Color

object RepositoryButtonPanes {

    private val shapeButtonsPane by lazy {
        FlowPane().apply {
            prefWidth = SHAPES_PANE_WIDTH
            border = BORDER
        }
    }
    private val shapeButtons by lazy { RepositoryImageButtons.getShapeButtonList() }
    private val colorButtonsPane by lazy { FlowPane().apply {
            prefWidth = COLORS_PANE_WIDTH
            hgap = COLOR_BUTTONS_SPACING
            vgap = COLOR_BUTTONS_SPACING
            padding = Insets(1.0, 1.0, 1.0, 1.0)
            border = BORDER
        }
    }
    private val colorButtons by lazy { RepositoryImageButtons.getColorButtonList() }
    private val modeButtonsPane by lazy { VBox().apply{
            border = BORDER
            prefWidth = COLORS_PANE_WIDTH
            padding = Insets(1.0, 1.0, 1.0, 1.0)
        }
    }
    private val modeButtons by lazy { RepositoryImageButtons.getModeButtonList() }
    private fun Button.hover() = EventHandler<MouseEvent> { this.effect = InnerShadow() }
    private fun Button.leave() = EventHandler<MouseEvent> { this.effect = null }
    private fun ShapeButton.onclick() = EventHandler<ActionEvent> {
        ArtStudioData.activeButton.isActive = false
        ArtStudioData.isShapeDrawing = false
        ArtStudioData.activeButton = this
        ArtStudioData.isFilling = this.name.endsWith("_filled")
        ArtStudioData.activeButton.isActive = true
    }
    private fun ColorButton.onclick(footerColor: Label?, footerColorBrick: Label?) = EventHandler<ActionEvent> {
        ArtStudioData.activeColor = color
        footerColor?.text = "color:${MathCalc.colorToHex(color)}  "
        footerColorBrick?.background = Background(BackgroundFill(color, null, null))
    }
    private fun ModeButton.onclick(canvasPane: Pane, morphingPane: MorphingPane) = EventHandler<ActionEvent> {
        when (this.mode) {
            ModeType.DRAWING -> {
                canvasPane.setPrefSize(1500.0, 1000.0)
                morphingPane.setPrefSize(1500.0, 0.0)
                morphingPane.border = null
                canvasPane.isVisible = true
                morphingPane.isVisible = false
                ArtStudioData.activeMode = ModeType.DRAWING
            }
            ModeType.MORPHING -> {
                canvasPane.setPrefSize(1500.0, 0.0)
                morphingPane.setPrefSize(1500.0, 1000.0)
                canvasPane.isVisible = false
                morphingPane.isVisible = true
                ArtStudioData.activeMode = ModeType.MORPHING
            }
        }
    }
    fun getShapePane(): Pane {
        shapeButtonsPane.children.addAll(shapeButtons.map { btn ->
            with(btn) {
                onAction = onclick()
                onMouseMoved = hover()
                onMouseExited = leave()
            }
            btn
        })
        if (shapeButtons.isNotEmpty()) {
            shapeButtons.first().isActive = true
            ArtStudioData.activeButton = shapeButtons.first()
        }
        return shapeButtonsPane
    }
    fun getColorPane(footerColor: Label?, footerColorBrick: Label?): Pane =
        colorButtonsPane.apply{
            children.addAll(colorButtons.map { clrBtn->
                with(clrBtn) {
                    onAction = onclick(footerColor, footerColorBrick)
                    onMouseMoved = hover()
                    onMouseExited = leave()
                }
                clrBtn
        })
    }
    fun getModePane(canvasPane: Pane, morphingPane: MorphingPane): VBox {
        val group = ToggleGroup()
        modeButtonsPane.children.addAll(modeButtons.map{
            it.toggleGroup = group
            it.onAction = it.onclick(canvasPane, morphingPane)
            it
        })
        return modeButtonsPane
    }

    private val BORDER = Border(BorderStroke(Color.BLUE, BorderStrokeStyle.SOLID, null, BorderWidths(1.0)))
    private const val SHAPES_PANE_WIDTH = 147.0
    private const val COLORS_PANE_WIDTH = 129.0
    private const val COLOR_BUTTONS_SPACING = 1.0
}