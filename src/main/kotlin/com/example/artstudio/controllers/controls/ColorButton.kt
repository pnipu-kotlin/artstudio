package com.example.artstudio.controllers.controls

import javafx.scene.control.Button
import javafx.scene.layout.*
import javafx.scene.paint.Color

class ColorButton(private var name: String, var color: Color) : Button("", null) {

    init {
        background = Background(BackgroundFill(color, null, null))
        border = BORDER
        prefWidth = WIDTH
        prefHeight = HEIGHT
        minHeight = HEIGHT
    }

    override fun toString(): String = "ColorButton(name='$name', color=$color)"

    private companion object {
        val BORDER = Border(BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, null, BorderWidths(1.0)))
        const val HEIGHT = 18.0
        const val WIDTH = 20.0
    }
}