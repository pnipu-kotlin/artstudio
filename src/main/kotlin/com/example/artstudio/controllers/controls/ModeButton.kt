package com.example.artstudio.controllers.controls

import com.example.artstudio.controllers.entities.ModeType
import javafx.scene.control.RadioButton

class ModeButton(caption: String, var mode: ModeType): RadioButton(caption)