package com.example.artstudio.controllers.messager

enum class MsgLevel {
    INFO,
    WARNING,
    ERROR
}