package com.example.artstudio.controllers.messager

import javax.swing.JOptionPane

class PaneMessager : IMessager {

    override fun info(msg: String): Int {
        return showMsg(msg, MsgLevel.INFO)
    }

    override fun warn(msg: String): Int {
        return showMsg(msg, MsgLevel.WARNING)
    }

    override fun error(msg: String): Int {
        return showMsg(msg, MsgLevel.ERROR)
    }

    private fun showMsg(msg: String, level: MsgLevel): Int{
        val title = when(level){
            MsgLevel.ERROR -> "Ошибка!"
            MsgLevel.WARNING -> "Внимание!"
            else -> ""
        }
        JOptionPane.showMessageDialog(null, msg, title, JOptionPane.OK_OPTION)
        return JOptionPane.OK_OPTION
    }
}