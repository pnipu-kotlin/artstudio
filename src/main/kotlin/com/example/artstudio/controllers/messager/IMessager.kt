package com.example.artstudio.controllers.messager

interface IMessager {
    fun info(msg: String): Int?
    fun warn(msg: String): Int?
    fun error(msg: String): Int?
}