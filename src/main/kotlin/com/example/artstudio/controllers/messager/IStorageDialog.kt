package com.example.artstudio.controllers.messager

import java.io.File

interface IStorageDialog {

    fun openDialog(title: String = "Открыть файл"): File?

    fun saveDialog(title: String = "Сохранить файл", fileNameDefault: String) : File?

}