package com.example.artstudio.controllers.messager

import com.example.artstudio.presenters.Helper

class TerminalMessager : IMessager {

    override fun info(msg: String): Int {
        showMsg(msg, MsgLevel.INFO)
        return 0
    }

    override fun warn(msg: String): Int {
        showMsg(msg, MsgLevel.WARNING)
        return 0
    }

    override fun error(msg: String): Int {
        showMsg(msg, MsgLevel.ERROR)
        return 0
    }

    private fun showMsg(msg: String, status: MsgLevel) {
        if (msg.isEmpty()) { return }
        println("${Helper.getNowDtString()} $status $msg")
    }


}