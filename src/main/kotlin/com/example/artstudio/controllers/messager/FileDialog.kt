package com.example.artstudio.controllers.messager

import javafx.stage.FileChooser
import java.io.File

class FileDialog : IStorageDialog {
    override fun openDialog(title: String): File? = makeChooser(title).showOpenDialog(null)

    override fun saveDialog(title: String, fileNameDefault: String): File?  = makeChooser(title).apply{
        initialFileName = fileNameDefault}.run{showSaveDialog(null)}

    private fun makeChooser(titl: String): FileChooser = FileChooser().apply {
        title = titl
        extensionFilters.add(
            FileChooser.ExtensionFilter("Figure Files", FIGURE_MASK_DEFAULT)
        )
    }

    companion object {
        private const val FIGURE_MASK_DEFAULT = "*.fig"
    }
}