package com.example.artstudio.controllers.entities

import javafx.scene.input.MouseEvent
import javafx.scene.paint.Color
import javafx.scene.shape.Ellipse

class Elli : Ellipse(), IShapeExt {

    private val ex = ShapeExt()
    private var centerX0 = 0.0
    private var centerY0 = 0.0

    override fun init(pList: MutableList<Point>, newColor:Color, id: Int){
        ex.init(newColor, id)
        while (pList.size < 2) {
            pList.add(Point(0,0))
        }
        centerX = pList.first().x.toDouble()
        centerY = pList.first().y.toDouble()
        radiusX = pList[1].x.toDouble()
        radiusY = pList[1].y.toDouble()
        stroke = newColor
    }

    override fun onMousePress(event: MouseEvent) {
        ex.onMousePress(event)
        centerX0 = centerX
        centerY0 = centerY
    }
    override fun onMouseDrag(event: MouseEvent) {
        val dxy = ex.onMouseDrag(event)
        centerX = centerX0 + dxy.x
        centerY = centerY0 + dxy.y
    }

    override fun onMouseRelease(){
        val color0 = ex.onMouseRelease()
        color0?.let {
            stroke = color0
            if (strokeDashArray.size > 1) {
                strokeDashArray[1] = 0.0
            }
        }
    }

    override fun onMouseClick(event: MouseEvent) {
        val colorDash = ex.onMouseClick(event)
        colorDash?.let {
            stroke = colorDash.first
            if (strokeDashArray.size > 1) {
                strokeDashArray[1] = colorDash.second
            }
        }
    }

    override fun getShapeId(): Int{
        return ex.id
    }

    override fun toString(): String {
        val sb = StringBuilder("Elli[")
        sb.append("centerX=").append(centerX)
        sb.append("; centerY=").append(centerY)
        sb.append("; radiusX=").append(radiusX)
        sb.append("; radiusY=").append(radiusY)
        sb.append("; fill=").append(fill)
        stroke?.let {
            sb.append("; stroke=").append(stroke)
            sb.append("; strokeWidth=").append(strokeWidth)
        }?:{
            sb.append("; stroke=").append(Color.web("#000000"))
            sb.append("; strokeWidth=").append(0.1)
        }

        return sb.append("]").toString();
    }
}