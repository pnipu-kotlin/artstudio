package com.example.artstudio.controllers.entities

import javafx.scene.input.MouseEvent
import javafx.scene.paint.Color

interface IShapeExt {
    fun init(pList: MutableList<Point>, newColor: Color, id: Int)
    fun onMousePress(event: MouseEvent)
    fun onMouseDrag(event: MouseEvent)
    fun onMouseRelease()
    fun onMouseClick(event: MouseEvent)
    fun getShapeId(): Int
}