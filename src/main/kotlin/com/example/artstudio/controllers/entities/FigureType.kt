package com.example.artstudio.controllers.entities

enum class FigureType {
    LINE,
    RECTANGLE,
    TRIANGLE,
    STAR5,
    ELLIPSE,
}