package com.example.artstudio.controllers.entities

enum class ModeType {
    DRAWING,
    MORPHING
}