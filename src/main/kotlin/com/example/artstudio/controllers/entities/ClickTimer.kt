package com.example.artstudio.controllers.entities

import java.time.LocalDateTime

class ClickTimer {

    private var timeStamp = 0
    fun start() {
        timeStamp = getTimeStamp()
    }
    private fun stop(): Int{
        val newTimeStamp = getTimeStamp()
        return if (newTimeStamp - timeStamp <0) newTimeStamp - timeStamp + 60000 else newTimeStamp - timeStamp
    }
    fun isClick(): Boolean {
        return  stop() <= MAX_CLICK_DELAY
    }

    private fun getTimeStamp(): Int {
        val nowTim = LocalDateTime.now()
        return nowTim.second *1000 + nowTim.nano / 1000000
    }
    private companion object{
        const val MAX_CLICK_DELAY = 300
    }
}