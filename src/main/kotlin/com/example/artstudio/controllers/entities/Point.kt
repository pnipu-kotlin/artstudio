package com.example.artstudio.controllers.entities

data class Point(var x: Int = 0, var y: Int = 0)
