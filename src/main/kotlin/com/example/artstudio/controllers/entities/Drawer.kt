package com.example.artstudio.controllers.entities
import javafx.scene.shape.Ellipse
import javafx.scene.shape.Polygon
import javafx.scene.shape.Shape

object Drawer {

    fun draw(shape: Shape, pList: MutableList<Point>) {
        if (pList.size < 2) {
            return
        }
        when (shape){
            is Ellipse -> {
                with(shape) {
                    centerX = pList.first().x.toDouble()
                    centerY = pList.first().y.toDouble()
                    radiusX = pList[1].x.toDouble()
                    radiusY = pList[1].y.toDouble()
                }
            }
            is Polygon -> {
                with(shape) {
                    points.clear()
                    pList.forEach { point ->
                        points.add(point.x.toDouble())
                        points.add(point.y.toDouble())
                    }
                }
            }
        }
    }
}
