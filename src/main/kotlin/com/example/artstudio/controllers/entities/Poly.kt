package com.example.artstudio.controllers.entities

import javafx.scene.input.MouseEvent
import javafx.scene.paint.Color
import javafx.scene.shape.Polygon

class Poly : Polygon(), IShapeExt {

    private val ex = ShapeExt()
    private var points0 = mutableListOf<Double>()

    override fun init(pList: MutableList<Point>, newColor: Color, id: Int) {
        ex.init(newColor, id)
        while (pList.size < 2) {
            pList.add(Point(0, 0))
        }
        points.clear()
        pList.forEach { point ->
            points.add(point.x.toDouble())
            points.add(point.y.toDouble())
        }
        stroke = newColor
    }
    /**
     * На нажатии фигуры запоминаем точку клика и начальное расположение узлов фигуры
     */
    override fun onMousePress(event: MouseEvent) {
        ex.onMousePress(event)
        points0.clear()
        points.forEach { point ->
            points0.add(point)
        }
    }
    override fun onMouseDrag(event: MouseEvent) {
        val dxy = ex.onMouseDrag(event)
        points.withIndex().forEach { (i, point) ->
            points[i] = points0[i] + dxy.y * (i % 2) + dxy.x * ((i + 1) % 2)
        }
    }
    override fun onMouseRelease() {
        val color0 = ex.onMouseRelease()
        color0?.let {
            stroke = color0
            if (strokeDashArray.size > 1) {
                strokeDashArray[1] = 0.0
            }
        }
    }
    override fun onMouseClick(event: MouseEvent) {
        val colorDash = ex.onMouseClick(event)
        colorDash?.let {
            stroke = colorDash.first
            if (strokeDashArray.size > 1) {
                strokeDashArray[1] = colorDash.second
            }
        }
    }
    override fun getShapeId(): Int{
        return ex.id
    }
    override fun toString(): String  {
        val sb = StringBuilder("Poly[")
        sb.append("points=").append(points)
        sb.append("; fill=").append(fill)
        stroke?.let{
            sb.append("; stroke=").append(stroke)
            sb.append("; strokeWidth=").append(strokeWidth)
        }?:{
            sb.append("; stroke=").append(Color.web("#000000"))
            sb.append("; strokeWidth=").append(1.0)
        }

        return sb.append("]").toString()
    }
}