package com.example.artstudio.controllers.entities

import com.example.artstudio.controllers.ArtStudioData
import javafx.scene.input.MouseEvent
import javafx.scene.paint.Color

class ShapeExt {

    private var x0: Double = 0.0
    private var y0: Double = 0.0
    private var color0 = Color.BLACK
    private var isDrag = false
    private val clickTimer by lazy { ClickTimer() }
    var id = 0

    fun init(newColor: Color, id: Int) {
        this.id = id
        color0 = newColor
    }
    /**
     * На нажатии фигуры запоминаем точку клика и начальное расположение узлов фигуры. Заодно сбрасывает event
     */
    fun onMousePress(event: MouseEvent) {
        x0 = event.x
        y0 = event.y
        clickTimer.start()
        if (isDrag) {
            event.consume()
        }
    }
    /**
     * На перетаскивании пересчитывает смещение мыши от начальной точки и возвращает их в Point, заодно сбрасывает event
     */
    fun onMouseDrag(event: MouseEvent): Point {
        val dx = (if (isDrag) (event.x - x0) else 0).toInt()
        val dy = (if (isDrag) (event.y - y0) else 0).toInt()
        if (isDrag) {
            event.consume()
        }
        return Point(dx, dy)
    }
    /**
     * Возвращает начальный цвет контура чтобы на выходе вернуть цвет контура и убрать пунктир
     * А если это был не релиз, а простой клик и сюда зашли по ошибке - возвращается null
     */
    fun onMouseRelease(): Color? {
        if (clickTimer.isClick()) {
            return null
        }
        isDrag = false
        ArtStudioData.activeId = 0
        return color0
    }
    /**
     * Возвращает контрастный цвет контура и пунктир если это был первый клик
     * либо начальный цвет и сплошной контур если второй клик
     * А если сюда зашли по ошибке и это не клик - возвращаем null
     */
    fun onMouseClick(event: MouseEvent): Pair<Color, Double>? {
        if (!clickTimer.isClick()) {
            return null
        }
        // Если дольше - игнорировать, ибо это не клик вовсе, а простой Release
        event.consume()
        // Переключаем режим рисование/таскание
        isDrag = !isDrag
        if (!isDrag) {
            ArtStudioData.activeId = 0
            return Pair(color0, 0.0)
        }
        ArtStudioData.whoDragging(id)
        var redContrast = (color0.red * 255 + 128).toInt()
        if (redContrast > 255) {
            redContrast -= 256
        }
        return Pair(
            Color.rgb(
                redContrast,
                (color0.green * 255).toInt(),
                (color0.blue * 255).toInt()
            ), 5.0
        )
    }
    override fun toString(): String {
        return "ShapeExt(x0=$x0, y0=$y0, color0=$color0, isDrag=$isDrag, clickTimer=$clickTimer, id=$id)"
    }
}