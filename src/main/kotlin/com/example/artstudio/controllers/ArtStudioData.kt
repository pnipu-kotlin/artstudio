package com.example.artstudio.controllers

import com.example.artstudio.controllers.controls.ShapeButton
import com.example.artstudio.controllers.entities.FigureType
import com.example.artstudio.controllers.entities.IShapeExt
import com.example.artstudio.controllers.entities.ModeType
import com.example.artstudio.controllers.entities.Point
import com.example.artstudio.presenters.ShapeFactory
import com.example.artstudio.presenters.calcs.CalcFactory
import javafx.scene.control.MenuItem
import javafx.scene.layout.Pane
import javafx.scene.paint.Color
import javafx.scene.shape.Shape

object ArtStudioData {

    var fileName = ""
    var menuSave: MenuItem? = null
    var shapesMap = HashMap<Int, Shape>()
    var startPoint = Point(0, 0)
    var activeCalc = CalcFactory.make(FigureType.LINE)
    var activeButton = ShapeButton("", "", FigureType.LINE)
    var activeColor: Color = Color.BLACK
    var isFilling = false
    var isShapeDrawing = false
    var activeMode = ModeType.DRAWING
    var activeFigure: Shape =
        ShapeFactory.make(FigureType.LINE, mutableListOf(startPoint), activeColor, isFilling, 0)
    var activeId = 0
    var mainCanvas: Pane? = null

    fun whoDragging(idClick: Int) {

        shapesMap.filter{(id, shape) -> id != idClick}.forEach{ (id, shape) ->
            if (shape is IShapeExt) shape.onMouseRelease()
        }
        activeId = idClick
    }
}