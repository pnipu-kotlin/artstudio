package com.example.artstudio

import com.example.artstudio.controllers.ArtStudioData
import com.example.artstudio.controllers.controls.MorphingPane
import com.example.artstudio.controllers.controls.RepositoryButtonPanes
import com.example.artstudio.presenters.MenuActions
import com.example.artstudio.presenters.PaneInitializer
import javafx.event.EventHandler
import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.control.Label
import javafx.scene.control.MenuBar
import javafx.scene.control.MenuItem
import javafx.scene.layout.*
import javafx.scene.paint.Color
import java.net.URL
import java.util.*


class ArtStudioController : Initializable {

    @FXML
    private var menuBar: MenuBar? = null
    @FXML
    private var footer: HBox? = null
    @FXML
    private var footerX: Label? = null
    @FXML
    private var footerY: Label? = null
    @FXML
    private var footerColor: Label? = null
    @FXML
    private var footerColorBrick: Label? = null
    @FXML
    private var menuHbox: HBox? = null
    @FXML
    private var menuNew: MenuItem? = null
    @FXML
    private var menuLoad: MenuItem? = null
    @FXML
    private var menuPng: MenuItem? = null
    @FXML
    private var menuSave: MenuItem? = null
    @FXML
    private var menuSaveAs: MenuItem? = null
    @FXML
    private var menuFront: MenuItem? = null
    @FXML
    private var menuBack: MenuItem? = null
    @FXML
    private var menuExit: MenuItem? = null
    @FXML
    private var menuDel: MenuItem? = null
    @FXML
    private var canvasVbox: StackPane? = null

    private val canvasPane by lazy { Pane() }
    private val morphingPane by lazy { MorphingPane().apply { initialize(null, null) } }
    private val shapeButtonsPane by lazy { RepositoryButtonPanes.getShapePane() }
    private val colorButtonsPane by lazy { RepositoryButtonPanes.getColorPane(footerColor, footerColorBrick) }
    private val modeButtonsPane by lazy { RepositoryButtonPanes.getModePane(canvasPane, morphingPane) }
    private val menuActions by lazy { MenuActions() }
    private val paneInitializer by lazy { PaneInitializer() }

    override fun initialize(p0: URL?, p1: ResourceBundle?) {
        ArtStudioData.mainCanvas = canvasPane
        menuNew?.onAction = EventHandler { menuActions.newAction(canvasPane) }
        menuLoad?.onAction = EventHandler { menuActions.loadAction(canvasPane) }
        menuSave?.let {
            it.onAction = EventHandler { menuActions.saveAction(canvasPane) }
            it.isDisable = true
            ArtStudioData.menuSave = it
        }
        menuSaveAs?.onAction = EventHandler {menuActions.saveAsAction(canvasPane) }
        menuPng?.onAction = EventHandler {menuActions.exportPngAction(canvasPane) }
        menuExit?.onAction = EventHandler { menuActions.exitAction() }
        menuFront?.onAction = EventHandler {menuActions.frontAction(canvasPane) }
        menuBack?.onAction = EventHandler {menuActions.backAction(canvasPane) }
        menuDel?.onAction = EventHandler {menuActions.delAction(canvasPane) }

        paneInitializer.initPane(canvasPane, -1, footerX, footerY)
        paneInitializer.initPane(morphingPane.paneLeft, 1, footerX, footerY)
        paneInitializer.initPane(morphingPane.paneRight, 1, footerX, footerY)

        footer?.run {
            viewOrder = -1.0
            background = BACK
        }
        menuBar?.run {
            viewOrder = -1.0
            background = BACK
        }
        menuHbox?.run {
            viewOrder = -1.0
            background = BACK
            spacing = CONTROL_PANES_SPACING
            children?.run {
                add(shapeButtonsPane)
                add(colorButtonsPane)
                add(modeButtonsPane)
            }
        }
        canvasVbox?.run {
            children.add(canvasPane)
            children.add(morphingPane)
        }

    }
    private companion object {
        const val CONTROL_PANES_SPACING = 20.0
        val BACK = Background(BackgroundFill(Color.web("#eeeeee"), null, null))
    }
}
