package com.example.artstudio.presenters

import com.example.artstudio.controllers.messager.TerminalMessager
import javafx.scene.shape.Shape

class ShapeNativeParser {

    fun parse(str: String?): Shape?{
        if (str.isNullOrEmpty()) { return null }
        return ShapeX(getMap(str)).getShape()

    }

    private fun getMap(str: String): Map<String, String>{
        val template = mutableMapOf(
            "fill" to "",
            "stroke" to "",
            "strokeWidth" to "",
            "points" to "[]",
            "centerX" to "",
            "centerY" to "",
            "radiusX" to "",
            "radiusY" to "",
            "ids" to ""+generateId()
        )
        try{
            val strWs = str.replace(" ","")
            val data = strWs.substring(strWs.indexOf("[")+1, strWs.lastIndexOf("]"))
            data.split(";" ).associate{
                val (left, right) = it.split("=")
                template[left] = right
                left to right
            }
        }catch(_: Exception){
            TerminalMessager().error("Не удалось распарсить фигуру $str")
        }
        return template
    }

    private fun generateId(): Int = (Math.random() * 10000000).toInt()
}