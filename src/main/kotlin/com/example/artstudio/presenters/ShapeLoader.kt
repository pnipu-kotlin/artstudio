package com.example.artstudio.presenters

import com.example.artstudio.controllers.ArtStudioData
import com.example.artstudio.controllers.messager.FileDialog
import com.example.artstudio.storage.FileStorage
import com.example.artstudio.storage.IOStatus
import javafx.scene.shape.Shape

class ShapeLoader {

    private val shapeList by lazy { mutableListOf<Shape>() }
    private val storageDialog by lazy { FileDialog() }
    private val fileStorage by lazy { FileStorage() }

    fun loadShapes(): MutableList<Shape> {
        val selectedFile = storageDialog.openDialog() ?: return shapeList
        ArtStudioData.fileName = selectedFile.toString()
        ArtStudioData.menuSave?.isDisable = false
        val loadResult = fileStorage.load(selectedFile.toString())
        if (loadResult.second != IOStatus.OK || loadResult.first.isEmpty()) {
            return shapeList
        }
        // Десериализуем строку в список объектов
        return loadResult.first.mapNotNull { str -> ShapeNativeParser().parse(str) }.toMutableList()
    }
}