package com.example.artstudio.presenters

import com.example.artstudio.controllers.messager.IMessager
import com.example.artstudio.controllers.messager.TerminalMessager
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException

object  Helper {

    private val log: IMessager by lazy { TerminalMessager() }
    private val defFormatter: DateTimeFormatter by lazy { DateTimeFormatter.ofPattern("yyyyMMdd_HHmmSS") }

    fun getNowDtString(format: String? = null) :String{
        val formatSafe = if (format.isNullOrEmpty()) "yyyyMMdd_HHmmSS" else format
        val dt = LocalDateTime.now()
        val dtFormatter = try{
            DateTimeFormatter.ofPattern(formatSafe)
        } catch(pe: DateTimeParseException){
            log.error(pe.message.toString())
            defFormatter
        }

        return dt.format(dtFormatter)
    }
    fun makeFileNameDefault(ext: String = "fig") : String ="picture_${getNowDtString()}.${ext}"
}