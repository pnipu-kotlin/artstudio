package com.example.artstudio.presenters

import com.example.artstudio.controllers.entities.FigureType
import com.example.artstudio.controllers.entities.Point
import com.example.artstudio.controllers.messager.TerminalMessager
import javafx.scene.paint.Color
import javafx.scene.shape.Shape

class ShapeX(private val map: Map<String, String>) {
    private val fill by map
    private val stroke by map
    private val strokeWidth by map
    private val points by map
    private val centerX by map
    private val centerY by map
    private val radiusX by map
    private val radiusY by map
    private val ids by map

    private val id = ids.toInt()
    private val fil = hexToColor(fill)
    private val strok = hexToColor(stroke)
    private val strokeWid = try{strokeWidth.toDouble()}catch(_:Exception){1.0}
    private val pointIntList = points.substring(1, points.length-1).split(",")
        .filter{pointStr-> pointStr.isNotEmpty()}
        .map{it.toDouble().toInt()}
        .toMutableList()
    private val pList = pointIntList.withIndex()
        .filter{(i,xy)-> i%2==1}
        .map{(i,xy)-> Point(pointIntList[i-1], xy)}
        .toMutableList()

    private val cX = try{centerX.toDouble().toInt()} catch(_:Exception){null}
    private val cY = try{centerY.toDouble().toInt()} catch(_:Exception){null}
    private val rX = try{radiusX.toDouble().toInt()} catch(_:Exception){null}
    private val rY = try{radiusY.toDouble().toInt()} catch(_:Exception){null}

    fun getShape(): Shape? {
        if (pList.isNotEmpty()) {
            return ShapeFactory.make(
                FigureType.LINE,
                pList,
                strok,
                isFilling(fil),
                id)
        }

        if (cX != null && cY != null && rX != null && rY != null){
            return ShapeFactory.make(
                FigureType.ELLIPSE,
                mutableListOf(Point(cX, cY), Point(rX, rY)),
                strok,
                isFilling(fil),
                id)
        }

        TerminalMessager().error("Неизвестный тип фигуры $map")
        return null
    }

    private fun hexToColor(colorHex: String): Color{
        if (colorHex.startsWith("0x")) {
            val hex = colorHex.replace("0x","#")
            try{
                return Color.web(hex)
            }catch(_:Exception){}
        }
        return Color.BLACK
    }

    private fun isFilling(clr: Color): Boolean{
        return clr.opacity != 0.0
    }
}