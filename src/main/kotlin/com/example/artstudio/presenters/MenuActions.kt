package com.example.artstudio.presenters

import com.example.artstudio.controllers.ArtStudioData
import com.example.artstudio.controllers.entities.IShapeExt
import com.example.artstudio.controllers.messager.ConfirmMessager
import com.example.artstudio.controllers.messager.FileDialog
import com.example.artstudio.storage.FileStorage
import javafx.collections.ObservableList
import javafx.scene.Node
import javafx.scene.SnapshotParameters
import javafx.scene.layout.Pane
import javafx.scene.shape.Polygon
import javax.swing.JOptionPane
import kotlin.system.exitProcess

class MenuActions {

    private val shapeLoader by lazy { ShapeLoader() }

    fun newAction(canvasPane: Pane?) {
        canvasPane?.children?.clear()
        with(ArtStudioData) {
            shapesMap.clear()
            fileName = ""
            menuSave?.isDisable = true
        }
    }

    fun loadAction(canvasPane: Pane?) {
        canvasPane?.run {
            val shapeList = shapeLoader.loadShapes()
            if (shapeList.isNotEmpty()) {
                ArtStudioData.shapesMap.clear()
                children.clear()
                // Переложим фигуры в канвас, сохраняя в них полученное положение в списке
                shapeList.forEach { shape ->
                    var id = -1
                    if (shape is IShapeExt) {
                        id = shape.getShapeId()
                    }
                    children.add(shape)
                    ArtStudioData.shapesMap[id] = shape
                }
            }
        }
    }

    fun exportPngAction(canvasPane: Pane?) {
        canvasPane?.run {
            val fileNameDef = Helper.makeFileNameDefault("png")
            val fileName = FileDialog().saveDialog("Экспорт рисунка в PNG формат", fileNameDef) ?: return

            val img = this.snapshot(SnapshotParameters(), null)
            FileStorage().exportToPng(fileName, img)
        }
    }

    fun saveAsAction(canvasPane: Pane?) {
        canvasPane?.run {
            val saveData = children.map { shape ->
                shape.toString()
            }.toMutableList()

            // Сгенерили название файла по умолчанию
            val fileNameDef = Helper.makeFileNameDefault("fig")
            val fileName = FileDialog().saveDialog("Сохранение рисунка", fileNameDef) ?: return
            ArtStudioData.fileName = fileName.toString()
            ArtStudioData.menuSave?.isDisable = false
            FileStorage().save(fileName.toString(), saveData)
        }
    }

    fun saveAction(canvasPane: Pane?) {
        if (ArtStudioData.fileName.isEmpty()) {
            return
        }
        canvasPane?.run {
            val saveData = children.map { shape ->
                shape.toString()
            }.toMutableList()
            // Сложили все фигуры в JSONArray и конвертировали в строку
            FileStorage().save(ArtStudioData.fileName, saveData)
        }
    }

    fun exitAction() {
        if (ConfirmMessager().info("Завершить работу?") == JOptionPane.OK_OPTION) {
            exitProcess(EXIT_STATUS_OK)
        }
    }

    fun frontAction(canvasPane: Pane?) {
        val id = ArtStudioData.activeId
        if (id == 0) {
            return
        }
        //ArtStudioData.shapesMap.remove(id)
        canvasPane?.run {
            val idx = indexof(children, id)
            if (idx >= 0 && idx < (children.size - 1)) {
                val shap1 = children[idx]
                val shap2 = children[idx + 1]
                children[idx + 1] = Polygon()
                children[idx] = shap2
                children[idx + 1] = shap1
            }

        }
    }

    private fun indexof(children: ObservableList<Node>, id: Int): Int {
        if (children.isEmpty()) {
            return -1
        }
        var i = 0
        while (i < children.size && (children[i] as IShapeExt).getShapeId() != id) {
            i++
        }
        if ((children[i] as IShapeExt).getShapeId() == id) {
            return i
        }
        return -1
    }

    fun backAction(canvasPane: Pane?) {
        val id = ArtStudioData.activeId
        if (id == 0) {
            return
        }
        //ArtStudioData.shapesMap.remove(id)
        canvasPane?.run {
            val idx = indexof(children, id)
            if (idx > 0) {
                val shap1 = children[idx]
                val shap2 = children[idx - 1]
                children[idx - 1] = Polygon()
                children[idx] = shap2
                children[idx - 1] = shap1
            }
        }
    }

    fun delAction(canvasPane: Pane?) {
        val id = ArtStudioData.activeId
        if (id == 0) {
            return
        }
        ArtStudioData.shapesMap.remove(id)
        canvasPane?.run {
            val shapes = children.filter { shape -> (shape as IShapeExt).getShapeId() != id }.toMutableList()
            this.children.clear()
            this.children.addAll(shapes)
        }
    }

    companion object {
        private const val EXIT_STATUS_OK = 0
        fun deleteAction() {
            val id = ArtStudioData.activeId
            if (id == 0) {
                return
            }
            ArtStudioData.shapesMap.remove(id)
            ArtStudioData.mainCanvas?.run {
                val shapes = children.filter { shape -> (shape as IShapeExt).getShapeId() != id }.toMutableList()
                this.children.clear()
                this.children.addAll(shapes)
            }
        }
    }
}