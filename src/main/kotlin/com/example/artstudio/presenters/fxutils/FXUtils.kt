package com.example.artstudio.presenters.fxutils

import javafx.scene.image.Image
import javafx.scene.image.PixelFormat
import javafx.scene.image.PixelReader
import javafx.scene.image.WritablePixelFormat
import java.awt.AlphaComposite
import java.awt.image.BufferedImage
import java.awt.image.DataBufferInt
import java.awt.image.SinglePixelPackedSampleModel
import java.nio.IntBuffer

object FXUtils {
    fun fromFXImage(var0: Image, var01: BufferedImage?): BufferedImage? {
        var var1 = var01
        val var2 = var0.pixelReader
        return if (var2 == null) {
            null
        } else {
            val var3 = var0.width.toInt()
            val var4 = var0.height.toInt()
            val var5 = var2.pixelFormat
            var var6 = false
            when (var5.type) {
                PixelFormat.Type.BYTE_BGRA_PRE, PixelFormat.Type.INT_ARGB_PRE, PixelFormat.Type.BYTE_BGRA, PixelFormat.Type.INT_ARGB -> if (var1 != null && (var1.type == 4 || var1.type == 1)) {
                    var6 = checkFXImageOpaque(var2, var3, var4)
                }
                else -> var6 = true
            }
            val var7 = getBestBufferedImageType(var2.pixelFormat, var1, var6)
            if (var1 != null) {
                val var8 = var1.width
                val var9 = var1.height
                if (var8 >= var3 && var9 >= var4 && var1.type == var7) {
                    if (var3 < var8 || var4 < var9) {
                        val var10 = var1.createGraphics()
                        var10.composite = AlphaComposite.Clear
                        var10.fillRect(0, 0, var8, var9)
                        var10.dispose()
                    }
                } else {
                    var1 = null
                }
            }
            if (var1 == null) {
                var1 = BufferedImage(var3, var4, var7)
            }
            val var14 = var1.raster.dataBuffer as DataBufferInt
            val var15 = var14.getData()
            val var16 = var1.raster.dataBuffer.offset
            var var11 = 0
            val var12 = var1.raster.sampleModel
            if (var12 is SinglePixelPackedSampleModel) {
                var11 = var12.scanlineStride
            }
            val var13: WritablePixelFormat<IntBuffer> = PixelFormat.getIntArgbPreInstance()
            var2.getPixels(0, 0, var3, var4, var13, var15, var16, var11)
            var1
        }
    }

    private fun checkFXImageOpaque(var0: PixelReader, var1: Int, var2: Int): Boolean {
        for (var3 in 0 until var1) {
            for (var4 in 0 until var2) {
                val var5 = var0.getColor(var3, var4)
                if (var5.opacity != 1.0) {
                    return false
                }
            }
        }
        return true
    }
    private fun getBestBufferedImageType(var0: PixelFormat<*>, var1: BufferedImage?, var2: Boolean): Int {
        if (var1 != null) {
            val var3 = var1.type
            if (var3 == 2 || var3 == 3 || var2 && (var3 == 4 || var3 == 1)) {
                return var3
            }
        }
        return when (var0.type) {
            PixelFormat.Type.BYTE_BGRA_PRE, PixelFormat.Type.INT_ARGB_PRE -> 3
            PixelFormat.Type.BYTE_BGRA, PixelFormat.Type.INT_ARGB -> 2
            PixelFormat.Type.BYTE_RGB -> 1
            PixelFormat.Type.BYTE_INDEXED -> if (var0.isPremultiplied) 3 else 2
            else -> 3
        }
    }
}