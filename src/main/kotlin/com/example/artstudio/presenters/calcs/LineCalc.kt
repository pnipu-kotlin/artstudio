package com.example.artstudio.presenters.calcs

import com.example.artstudio.controllers.entities.Point

class LineCalc : ICalc {
    // Рассчитывает полигон из двух точек

    override fun getPointsByBounds(pointFrom: Point, pointTo: Point): MutableList<Point> =
        mutableListOf(pointFrom, pointTo)
}