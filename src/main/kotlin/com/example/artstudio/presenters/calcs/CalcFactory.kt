package com.example.artstudio.presenters.calcs

import com.example.artstudio.controllers.entities.FigureType

object CalcFactory {

    fun make(typeShape: FigureType): ICalc = when (typeShape) {
        FigureType.LINE -> LineCalc()
        FigureType.RECTANGLE -> RectCalc()
        FigureType.TRIANGLE -> TriangleCalc()
        FigureType.STAR5 -> Star5Calc()
        FigureType.ELLIPSE -> EllipseCalc()
    }
}