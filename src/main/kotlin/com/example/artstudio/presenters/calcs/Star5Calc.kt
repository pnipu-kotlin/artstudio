package com.example.artstudio.presenters.calcs

import com.example.artstudio.controllers.entities.Point

class Star5Calc : ICalc {
    // Рассчитывает полигон пятиконечной звезды

    override fun getPointsByBounds(pointFrom: Point, pointTo: Point): MutableList<Point> = starPoints
        .map{ pair->
          calcPoint(pointFrom, pointTo, pair.first, pair.second)
    }.toMutableList()

    /**
     * Расчет точки по заданным границам описанного прямоугольника и относительным смещениям от начала
     */
    private fun calcPoint(point0: Point, point1: Point, dx: Double, dy: Double): Point = Point(
        ((point1.x - point0.x) * dx).toInt() + point0.x,
        ((point1.y - point0.y) * dy).toInt() + point0.y
    )

    private companion object {
        val starPoints = listOf(
            0.5 to 0.0,
            0.62 to 0.4,
            1.0 to 0.4,
            0.7 to 0.62,
            0.8 to 1.0,
            0.5 to 0.78,
            0.2 to 1.0,
            0.3 to 0.62,
            0.0 to 0.4,
            0.38 to 0.4
        )
    }
}