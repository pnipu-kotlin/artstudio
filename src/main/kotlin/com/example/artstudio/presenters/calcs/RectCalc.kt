package com.example.artstudio.presenters.calcs

import com.example.artstudio.controllers.entities.Point

class RectCalc : ICalc {
    // Рассчитывает полигон прямоугольника

    override fun getPointsByBounds(pointFrom: Point, pointTo: Point): MutableList<Point> = mutableListOf(
        pointFrom,
        Point(pointTo.x, pointFrom.y),
        pointTo,
        Point(pointFrom.x, pointTo.y)
    )
}