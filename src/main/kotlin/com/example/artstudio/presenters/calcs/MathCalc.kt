package com.example.artstudio.presenters.calcs

import com.example.artstudio.controllers.entities.Point
import javafx.scene.paint.Color
import javafx.scene.shape.Ellipse
import javafx.scene.shape.Polygon
import kotlin.math.cos
import kotlin.math.sin
import kotlin.math.sqrt

object MathCalc {

    // Наибольший общий делитель
    private fun nod(a: Int, b: Int): Int {
        if (b == 0) { return a }
        return nod(b, a % b)
    }
    // Наименьшее общее кратное
    fun nok(a: Int, b: Int): Int = a / nod(a, b) * b

    // Возвращает true если список имеет правильное направление (по часовой стрелке) в районе опорной точки
    private fun calcVectorProd(prev: Point, orig: Point, nex: Point): Boolean {
        // Направление обхода найдем по скалярному произведению векторов (Если >=0 - то по часовой стрелке)
        val x1 = orig.x - prev.x
        val y1 = orig.y - prev.y
        val x2 = nex.x - orig.x
        val y2 = nex.y - orig.y

        return (x1 * y2 - x2 * y1) <= 0
    }
    fun hypot(p1: Point, p2: Point): Double {
        val dx = (p2.x - p1.x) * (p2.x - p1.x).toDouble()
        val dy = (p2.y - p1.y) * (p2.y - p1.y).toDouble()
        return sqrt(dx + dy)
    }
    // Нахождение одной из самых правых точек полинома
    private fun getOriginIndex(pList: List<Point>): Int {
        var maxX = -10000
        var maxI = -1
        pList.withIndex().forEach { (i, point) ->
            if (point.x > maxX) {
                maxX = point.x
                maxI = i
            }
        }
        return maxI
    }
    /**
     * Дробим эллипс на замкнутый полигон из 100 отрезков
     */
    fun ellipseToPoly(elli: Ellipse): Polygon {
        // на входе координаты
        val cx = elli.centerX
        val cy = elli.centerY
        val rx = elli.radiusX
        val ry = elli.radiusY

        val pointList = mutableListOf<Double>()
        val deg = Math.PI * 2 / (MAX_POINTS_COUNT - 1)
        var al = 0.0
        while (al < Math.PI * 2) {
            pointList.add(cx + rx * cos(al))
            pointList.add(cy + ry * sin(al))
            al += deg
        }
        val poly = Polygon().apply {
            points.addAll(pointList)
            stroke = elli.stroke
            fill = elli.fill
        }
        return poly
    }
    fun orientPointsList(startPointList: List<Point>): MutableList<Point> {
        // Найдем самую правую точку - она будет стартовой для привязки
        // и следующую за ней по часовой стрелке - это будет направление привязки,
        // то есть найти главную точку привязки полигона, от которой будем вести отчет (надо будет сместить список)
        // и направление обхода (если оно окажется против шерсти - список надо развернуть)
        val origI = getOriginIndex(startPointList)
        val origPrev = if (origI == 0) (startPointList.size - 1) else (origI - 1)
        val origNext = if (origI == startPointList.size - 1) (0) else (origI + 1)

        val newPointList = mutableListOf<Point>()
        val step = if (calcVectorProd(startPointList[origPrev], startPointList[origI], startPointList[origNext])) { 1 } else {-1 }
        var i = origI
        do {
            newPointList.add(startPointList[i])
            i += step
            if (i >= startPointList.size) { i = 0 }
            if (i < 0) { i = startPointList.size - 1 }
        } while (i != origI)
        return newPointList
    }

    fun colorToHex(color: Color): String {
        val redx = color.red * BYTE_MAX
        val greenx = color.green * BYTE_MAX
        val bluex = color.blue * BYTE_MAX
        val rgbx = (((redx * BYTE_VAL + greenx) * BYTE_VAL) + bluex).toInt()
        val rgbHex = "000000" + rgbx.toString(16)
        return "#" + rgbHex.substring(rgbHex.length-6)
    }

    private const val MAX_POINTS_COUNT = 100
    private const val BYTE_MAX = 255
    private const val BYTE_VAL = 256
}