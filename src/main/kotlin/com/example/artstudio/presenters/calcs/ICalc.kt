package com.example.artstudio.presenters.calcs

import com.example.artstudio.controllers.entities.Point

interface ICalc {
    fun getPointsByBounds(pointFrom: Point, pointTo: Point): MutableList<Point>
}