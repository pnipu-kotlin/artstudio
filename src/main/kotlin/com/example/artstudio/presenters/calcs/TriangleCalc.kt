package com.example.artstudio.presenters.calcs

import com.example.artstudio.controllers.entities.Point

class TriangleCalc : ICalc {

    override fun getPointsByBounds(pointFrom: Point, pointTo: Point): MutableList<Point> = mutableListOf(
        pointFrom,
        pointTo,
        Point(pointFrom.x, pointTo.y)
    )
}