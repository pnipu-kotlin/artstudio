package com.example.artstudio.presenters.calcs

import com.example.artstudio.controllers.entities.Point
import javafx.scene.layout.Pane
import javafx.scene.paint.Color
import javafx.scene.shape.Polygon

object MorphingCalc {

    /**
     * Приведение количества точек фигуры к заданному
     */
    fun checkCount(pane: Pane, countTo: Int): Boolean {
        if (pane.children.isEmpty() || pane.children.first() !is Polygon) {
            return false
        }
        val fig = pane.children.first() as Polygon

        // Перед дальнейшими мероприятиями переложим россыпные координаты точек в List<Point>
        val startPointList = fig.points.withIndex().filter{(i, point) -> i.isOdd()}
            .map{ (i, point) ->
                Point(fig.points[i - 1].toInt(), point.toInt())
        }
        // Ориентируем список точек (начальная будет самой левой, а обход будет по часовой стрелке)
        val orientedPointList = MathCalc.orientPointsList(startPointList)

        // Теперь надо растянуть список до нужного количества точек.
        // Вставив в него точки пропорционально длинам сегментов
        // и заполнив интерполяционными данными
        val pointDoubleList = makeStrengList(orientedPointList, countTo)
            .flatMap { point ->
                listOf( point.x.toDouble(), point.y.toDouble() )
        }
        fig.points.clear()
        fig.points.addAll(pointDoubleList)
        return true
    }

    fun doMorphing(fig1: Polygon, fig2: Polygon, paneCenter: Pane, sliderVal: Int) {
        val fig3 = Polygon().apply {
            stroke = fig1.stroke
            fill = fig1.fill
        }

        // Промежуточные положения
        val points3List = fig1.points.withIndex().map { (i, point) ->
            (fig2.points[i] - point) * sliderVal / 100 + point
        }.toMutableList()
        fig3.points.addAll(points3List)
        with(paneCenter) {
            children.clear()
            children.add(fig3)
        }

        // Добавим изменение цветности границы
        val stroke1 = fig1.stroke as Color
        val stroke2 = fig2.stroke as Color
        val stroke12Arr = mutableListOf(
            stroke1.red to stroke2.red,
            stroke1.green to stroke2.green,
            stroke1.blue to stroke2.blue,
            stroke1.opacity to stroke2.opacity)
        val stroke3Arr = stroke12Arr.map { clrs ->
            (clrs.second - clrs.first) * sliderVal / 100 + clrs.first
        }
        fig3.stroke = Color(stroke3Arr[0], stroke3Arr[1], stroke3Arr[2], stroke3Arr[3])
        // Добавим изменение цветности границы
        fig3.stroke = calcColor(fig1.stroke as Color, fig2.stroke as Color, sliderVal)
        fig3.fill = calcColor(fig1.fill as Color, fig2.fill as Color, sliderVal)
    }

    private fun calcColor(clr1: Color, clr2: Color, sliderVal: Int): Color {
        val clr1Arr = mutableListOf(
            clr1.red to clr2.red,
            clr1.green to clr2.green,
            clr1.blue to clr2.blue,
            clr1.opacity to clr2.opacity)

        val clr3Arr = clr1Arr.map { c ->
            (c.second - c.first) * sliderVal / 100 + c.first
        }
        return Color(clr3Arr[0], clr3Arr[1], clr3Arr[2], clr3Arr[3])
    }

    private fun makeStrengList(pList: MutableList<Point>, countTo: Int): MutableList<Point> {
        // Создадим список длин сегментов
        val lenList = pList.withIndex().filter{ (i, point) -> i < pList.size - 1}
            .map{ (i, point) ->
                MathCalc.hypot(point, pList[i + 1])
        }.toMutableList()
        lenList.add(MathCalc.hypot(pList[0], pList[pList.size - 1]))

        // Распределим количество дополнительных точек пропорционально длинам сегментов
        val dCount = countTo - lenList.size
        val sum1 = lenList.sum()
        val dList = lenList.map { len ->
            Math.round(len * dCount / sum1).toInt()
        }.toMutableList()
        val sum2 = dList.sum()
        val dSum = dCount - sum2
        // Если остались нераспределенные точки, или наоборот перебор - находим максимальную длину,
        // и к ней добавляем одну точку (или вычитаем)
        if (dSum != 0) {
            val step = if (dSum > 0) { 1 } else { -1 }
            var maxLen = -1.0
            var maxInd = -1
            lenList.withIndex().forEach { (i, len) ->
                if (len > maxLen) {
                    maxLen = len
                    maxInd = i
                }
            }
            dList[maxInd] += step
            // Если и после этого не схождение - тупо всем подряд добавляем, пока излишки не кончатся
            var i = 0
            while (dList.sum() != dCount && i < dList.size) {
                dList[i] += step
                i++
            }
        }
        // Ну все уже, посчитали количество точек, надо распихивать в новый список
        return fillStrengList(pList, dList)
    }

    private fun fillStrengList(pList: MutableList<Point>, dList: MutableList<Int>): MutableList<Point> {
        val strengList = mutableListOf<Point>()
        pList.withIndex().forEach { (i, point) ->
            // Добавляем очередной основной узел
            strengList.add(point)
            // Добавляем дополнительные
            val cnt = dList[i]
            // в них надо сразу вписать интерполированные значения
            val endPoint = if (i == pList.size - 1) {
                pList[0]
            } else {
                pList[i + 1]
            }
            val dx = endPoint.x - point.x
            val dy = endPoint.y - point.y
            for (j in 1..cnt) {
                val newX = point.x + dx * j / (cnt + 1)
                val newY = point.y + dy * j / (cnt + 1)
                strengList.add(Point(newX, newY))
            }
        }
        return strengList
    }

    private fun Int.isEven(): Boolean = this.mod(2) == 0
    private fun Int.isOdd(): Boolean = !this.isEven()
}