package com.example.artstudio.presenters.calcs

import com.example.artstudio.controllers.entities.Point

class EllipseCalc : ICalc {

    override fun getPointsByBounds(pointFrom: Point, pointTo: Point): MutableList<Point> = mutableListOf(
        Point((pointTo.x + pointFrom.x) / 2, (pointTo.y + pointFrom.y) / 2),
        Point((pointTo.x - pointFrom.x) / 2, (pointTo.y - pointFrom.y) / 2)
    )
}