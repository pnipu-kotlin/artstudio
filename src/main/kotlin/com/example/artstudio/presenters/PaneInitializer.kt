package com.example.artstudio.presenters

import com.example.artstudio.controllers.ArtStudioData
import com.example.artstudio.controllers.entities.Drawer
import com.example.artstudio.controllers.entities.Point
import com.example.artstudio.presenters.calcs.CalcFactory
import javafx.event.EventHandler
import javafx.scene.control.Label
import javafx.scene.input.MouseEvent
import javafx.scene.layout.Pane

class PaneInitializer {

    fun initPane(canvasPane: Pane, maxShapeCount: Int, footX: Label?, footY: Label?) {
        canvasPane.apply {
            setPrefSize(1500.0, 1000.0)
            onMousePressed = EventHandler { event ->
                if (ArtStudioData.isShapeDrawing) {
                    return@EventHandler
                }
                // при старте рисования запрашиваем чертежника у фабрики по типу рисуемой линии
                // сначала определим, какую фигуру рисуем
                with(ArtStudioData) {
                    isShapeDrawing = true

                    startPoint = checkXY(event, footX, footY)
                    activeCalc = CalcFactory.make(activeButton.typeFigure)
                    val points = activeCalc.getPointsByBounds(
                        startPoint,
                        startPoint
                    )

                    var id = 0
                    while (id == 0 || shapesMap.containsKey(id)) {
                        // При генерации ИД фигуры убеждаемся, что оно уникальное на данный момент
                        id = (Math.random() * 10000000).toInt()
                    }
                    activeFigure = ShapeFactory.make(
                        activeButton.typeFigure, points,
                        activeColor,
                        isFilling, id
                    )

                    if (maxShapeCount > 0 && canvasPane.children.size >= maxShapeCount) {
                        canvasPane.children.remove(0, 1)
                    }
                    canvasPane.children?.add(activeFigure)
                    shapesMap[id] = activeFigure
                }
            }
            onMouseMoved = EventHandler { event ->
                checkXY(event, footX, footY)
            }
            onMouseDragged = EventHandler { event ->
                if (ArtStudioData.isShapeDrawing) {
                    // рассчитываем точки для текущего положения мыши
                    val points = ArtStudioData.activeCalc.getPointsByBounds(
                        ArtStudioData.startPoint,
                        checkXY(event, footX, footY)
                    )
                    Drawer.draw(ArtStudioData.activeFigure, points)
                }
            }
            onMouseReleased = EventHandler { _ ->
                // По отпускании мыши прекращаем рисование
                ArtStudioData.isShapeDrawing = false
                ArtStudioData.whoDragging(0)
            }

            viewOrder = 10.0
        }
    }

    private fun checkXY(event: MouseEvent, footerX: Label?, footerY: Label?): Point {
        val x = if (event.x.toInt() < 0) 0 else event.x.toInt()
        val y = if (event.y.toInt() < 0) 0 else event.y.toInt()
        footerX?.text = "X : $x"
        footerY?.text = "Y : $y"
        return Point(x, y)
    }
}