package com.example.artstudio.presenters

import com.example.artstudio.controllers.entities.Elli
import com.example.artstudio.controllers.entities.FigureType
import com.example.artstudio.controllers.entities.Point
import com.example.artstudio.controllers.entities.Poly
import javafx.event.EventHandler
import javafx.scene.paint.Color
import javafx.scene.shape.Shape

object ShapeFactory {

    fun make(
        typeFigure: FigureType,
        pList: MutableList<Point>,
        color: Color = Color.BLACK,
        isFilling: Boolean = false,
        id: Int
    ): Shape{

        while (pList.size < 2) {
            pList.add(Point(0, 0))
        }

        val fig: Shape = when (typeFigure) {
            FigureType.ELLIPSE ->
                Elli()
            else ->
                Poly()
        }.apply {
            init(pList, color, id)
            onMousePressed = EventHandler { event ->  onMousePress(event) }
            onMouseDragged = EventHandler { event ->  onMouseDrag(event) }
            onMouseReleased = EventHandler { onMouseRelease() }
            onMouseClicked = EventHandler { event -> onMouseClick(event) }

        }

        return fig.apply {
            stroke = color
            fill = if (!isFilling) Color.TRANSPARENT else color
            strokeDashArray.addAll(listOf(5.0, 0.0))
        }
    }
}