module com.example.artstudio {
    requires javafx.controls;
    requires javafx.graphics;
    requires javafx.base;
    requires javafx.fxml;
    requires kotlin.stdlib;

    requires java.desktop;

    opens com.example.artstudio to javafx.fxml;
    exports com.example.artstudio;
}